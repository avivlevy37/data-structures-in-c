#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif
#include "list.h"

#define INPUT(prompt, expected, format, ...) { \
	printf(prompt); \
	while (expected != scanf_s(format, __VA_ARGS__)) { \
		printf("Invalid input, try again!"); \
	} \
}
void printList(List* list);


int main(void) {
	int returnVal = EXIT_SUCCESS;
	List* list;
	ListNode* node = NULL;
	bool sanityCheck = true;
	int choice = 0;
	float value;

	if (NULL == (list = initList())) {
		printf("Memory allocation failed!\n");
		return EXIT_FAILURE;
	}
	
	while (sanityCheck) {
		printList(list);
		INPUT(
			"1. Add element\n"
			"2. Remove element\n"
			"3. Print average\n",
			1, "%d", &choice
		);
		switch (choice) {
		case 1:
			INPUT("Enter value: ", 1, "%f", &value);
			returnVal = addElement(list, value);
			if (EXIT_SUCCESS != returnVal)
				goto cleanup;
			break;
		case 2:
			INPUT("Enter value: ", 1, "%f", &value);
			node = getFirst(list);
			while (NULL != node) {
				if (getValue(node) == value)
					break;
				node = getNext(node);
			}
			if (NULL != node)
				removeElement(node);
			else
				printf("Value not found.\n");
			break;
		case 3:
			printf("%f\n", getAverage(list));
			break;
		default:
			sanityCheck = false;
		}
	}

cleanup:
	freeList(&list);
#if defined(_DEBUG)
	printf("Leaks detected: %d\n", _CrtDumpMemoryLeaks());
#endif
	return returnVal;
}

void printList(List* list) {
	bool printed = false;
	ListNode* node = getFirst(list);
	while (NULL != node) {
		printf("%.4f->", getValue(node));
		node = getNext(node);
		printed = true;
	}
	printf(printed ? "NULL\n" : "[Empty list]\n");
}
