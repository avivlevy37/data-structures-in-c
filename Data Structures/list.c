#include "list.h"

struct ListNode {
    float value;
    struct ListNode* prev, * next;
    struct List* list;
};
struct List {
    size_t numOfElems;
    struct ListNode* first, * last;
    float average;
};

List* initList(void)
{
    List* list = malloc(sizeof(List));
    if (NULL == list)
        return list;
    list->average = 0.0f;
    list->first = list->last = NULL;
    list->numOfElems = 0;
    return list;
}

void freeList(List** listPtr)
{
    List* list;
    size_t i = 0;
    ListNode* curr = NULL, * next = NULL;

    if (NULL == listPtr || NULL == (list = *listPtr))
        return;
        
    next = list->first;
    while (NULL != next) {
        curr = next;
        next = next->next;
        free(curr);
        ++i;
    }
    free(list);
    *listPtr = NULL;
}

int addElement(List* list, float value)
{
    ListNode* newNode;

    if (NULL == list)
        return EXIT_FAILURE;
    if (NULL == (newNode = malloc(sizeof(ListNode))))
        return errno;

    newNode->value = value;
    newNode->prev = list->last;
    newNode->next = NULL;
    newNode->list = list;

    if (0 != list->numOfElems) {
        list->last->next = newNode;
        list->last = newNode;
    }
    else {
        list->first = list->last = newNode;
    }
    ++list->numOfElems;
    list->average += (value - list->average) / list->numOfElems;

    return 0;
}

int removeElement(ListNode* elem)
{
    List* list = NULL;
    ListNode* prev = NULL, * next = NULL;

    if (NULL == elem)
        return EXIT_FAILURE;

    list = elem->list;
    prev = elem->prev;
    next = elem->next;

    if (NULL != prev) {
        prev->next = next;
    }
    if (NULL != next) {
        next->prev = prev;
    }
    if (NULL != list) {
        --list->numOfElems;
        list->average -= (elem->value - list->average) / list->numOfElems;

        if (elem == list->first)
            list->first = next;
        else if (elem == list->last)
            list->last = prev;
    }
    free(elem);

    return 0;
}

size_t getListLength(const List* list)
{
    if (NULL == list)
        return 0;
    return list->numOfElems;
}

ListNode* getFirst(const List* list)
{
    if (NULL == list)
        return NULL;
    return list->first;
}

ListNode* getLast(const List* list)
{
    if (NULL == list)
        return NULL;
    return list->last;
}

ListNode* getNext(const ListNode* node)
{
    if (NULL == node)
        return NULL;
    return node->next;
}

ListNode* getPrev(const ListNode* node)
{
    if (NULL == node)
        return NULL;
    return node->prev;
}

float getValue(const ListNode* node)
{
    if (NULL == node)
        return 0.0f;
    return node->value;
}

float getAverage(const List* list)
{
    if (NULL == list)
        return 0.0f;
    return list->average;
}
