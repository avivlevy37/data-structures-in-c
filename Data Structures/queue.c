#include "queue.h"
#include  "list.h"

struct Queue {
	List* list;
};

Queue* initQueue(void) {
	Queue* queue = malloc(sizeof(Queue));
	if (NULL == queue)
		return queue;
	queue->list = initList();
	return queue;
}

void freeQueue(Queue** queuePtr) {
	Queue* queue;
	if (NULL == queuePtr || NULL == (queue = *queuePtr))
		return;
	freeList(&queue->list);
	free(queue);
	*queuePtr = NULL;
}

int enqueue(Queue* queue, float value) {
	if (NULL == queue)
		return EXIT_FAILURE;
	return addElement(queue->list, value);
}

float dequeue(Queue* queue) {
	float value;
	ListNode* first;
	if (NULL == queue || 0 == getListLength(queue->list))
		return 0.0f;
	value = getValue(first = getFirst(queue->list));
	removeElement(first);
	return value;
}

size_t getQueueLength(Queue* queue) {
	if (NULL == queue)
		return 0;
	return getListLength(queue->list);
}

float peek(Queue* queue) {
	if (NULL == queue)
		return 0.0f;
	return getValue(getFirst(queue->list));
}