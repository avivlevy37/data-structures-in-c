#pragma once
#include "pc.h"

struct Queue;
typedef struct Queue Queue;

Queue* initQueue(void);

void freeQueue(Queue** queuePtr);

int enqueue(Queue* queue, float value);

float dequeue(Queue* queue);

size_t getQueueLength(Queue* queue);

float peek(Queue* queue);
