#pragma once
#include "pc.h"

struct ListNode;
struct List;
typedef struct ListNode ListNode;
typedef struct List List;

/*
Creates and returns an empty list.
*/
List* initList(void);

/*
Deletes all the elements from the list and the list itself.
Receives a double pointer to set the list to NULL.
*/
void freeList(List** listPtr);

/*
Adds value as an element to the end of the list.
Returns: Zero if the operation was successful, otherwise a non-zero value.
*/
int addElement(List* list, float value);

/*
Removes element from the list.
Returns: Zero if the operation was successful, otherwise a non-zero value.
*/
int removeElement(ListNode* element);

/*
Returns a list's length.
*/
size_t getListLength(const List* list);

/*
Returns a list's first element.
*/
ListNode* getFirst(const List* list);

/*
Returns a list's last element node.
*/
ListNode* getLast(const List* list);

/*
Returns the pointer to the node after the entered one.
*/
ListNode* getNext(const ListNode* node);

/*
Returns the pointer to the node before the entered one.
*/
ListNode* getPrev(const ListNode* node);

/*
Returns the value in the entered node.
*/
float getValue(const ListNode* node);

/*
Returns the average of values in the list.
*/
float getAverage(const List* list);
